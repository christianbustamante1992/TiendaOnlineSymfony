<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estadopedido
 *
 * @ORM\Table(name="estadopedido")
 * @ORM\Entity
 */
class Estadopedido
{
    /**
     * @var integer
     *
     * @ORM\Column(name="estadopedido_estado", type="integer", nullable=false)
     */
    private $estadopedidoEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="estadopedido_nombre", type="string", length=100, nullable=false)
     */
    private $estadopedidoNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="estadopedido_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $estadopedidoId;


}

