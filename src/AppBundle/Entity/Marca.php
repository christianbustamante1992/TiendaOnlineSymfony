<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Marca
 *
 * @ORM\Table(name="marca")
 * @ORM\Entity
 */
class Marca
{
    /**
     * @var integer
     *
     * @ORM\Column(name="marca_estado", type="integer", nullable=false)
     */
    private $marcaEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="marca_nombre", type="string", length=100, nullable=false)
     */
    private $marcaNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="marca_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $marcaId;


}

