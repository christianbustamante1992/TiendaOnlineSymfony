<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Producto
 *
 * @ORM\Table(name="producto", uniqueConstraints={@ORM\UniqueConstraint(name="producto_codigo", columns={"producto_codigo"})})
 * @ORM\Entity
 */
class Producto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_categoria", type="integer", nullable=true)
     */
    private $idCategoria;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_marca", type="integer", nullable=true)
     */
    private $idMarca;

    /**
     * @var string
     *
     * @ORM\Column(name="producto_codigo", type="string", length=100, nullable=false)
     */
    private $productoCodigo;

    /**
     * @var string
     *
     * @ORM\Column(name="producto_nombre", type="string", length=400, nullable=false)
     */
    private $productoNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="producto_descripcion", type="string", length=400, nullable=true)
     */
    private $productoDescripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="producto_stock", type="integer", nullable=true)
     */
    private $productoStock;

    /**
     * @var string
     *
     * @ORM\Column(name="producto_precioa", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $productoPrecioa;

    /**
     * @var string
     *
     * @ORM\Column(name="producto_preciob", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $productoPreciob;

    /**
     * @var string
     *
     * @ORM\Column(name="producto_fotoa", type="string", length=200, nullable=false)
     */
    private $productoFotoa;

    /**
     * @var string
     *
     * @ORM\Column(name="producto_fotob", type="string", length=200, nullable=false)
     */
    private $productoFotob;

    /**
     * @var string
     *
     * @ORM\Column(name="producto_fotoc", type="string", length=200, nullable=false)
     */
    private $productoFotoc;

    /**
     * @var integer
     *
     * @ORM\Column(name="producto_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productoId;


}

