<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pedido
 *
 * @ORM\Table(name="pedido", indexes={@ORM\Index(name="id_usuario", columns={"id_usuario"}), @ORM\Index(name="id_estado", columns={"id_estado"})})
 * @ORM\Entity
 */
class Pedido
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pedido_fecha", type="date", nullable=false)
     */
    private $pedidoFecha;

    /**
     * @var string
     *
     * @ORM\Column(name="pedido_total", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $pedidoTotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="pedido_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pedidoId;

    /**
     * @var \AppBundle\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="usuario_id")
     * })
     */
    private $idUsuario;

    /**
     * @var \AppBundle\Entity\Estadopedido
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Estadopedido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado", referencedColumnName="estadopedido_id")
     * })
     */
    private $idEstado;


}

