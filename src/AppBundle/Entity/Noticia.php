<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Noticia
 *
 * @ORM\Table(name="noticia", indexes={@ORM\Index(name="id_categoria", columns={"id_categoria"})})
 * @ORM\Entity
 */
class Noticia
{
    /**
     * @var string
     *
     * @ORM\Column(name="noticia_titulo", type="string", length=300, nullable=false)
     */
    private $noticiaTitulo;

    /**
     * @var string
     *
     * @ORM\Column(name="noticia_cuerpo", type="string", length=1000, nullable=false)
     */
    private $noticiaCuerpo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="noticia_fecha", type="date", nullable=false)
     */
    private $noticiaFecha;

    /**
     * @var integer
     *
     * @ORM\Column(name="noticia_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $noticiaId;

    /**
     * @var \AppBundle\Entity\Categorianoticia
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Categorianoticia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_categoria", referencedColumnName="categorianoticia_id")
     * })
     */
    private $idCategoria;


}

