<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Empleado
 *
 * @ORM\Table(name="empleado", uniqueConstraints={@ORM\UniqueConstraint(name="empleado_cedula", columns={"empleado_cedula"}), @ORM\UniqueConstraint(name="empleado_correo", columns={"empleado_correo"}), @ORM\UniqueConstraint(name="empleado_telefono", columns={"empleado_telefono"}), @ORM\UniqueConstraint(name="empleado_celular", columns={"empleado_celular"})}, indexes={@ORM\Index(name="id_estado", columns={"id_estado"}), @ORM\Index(name="id_cargo", columns={"id_cargo"})})
 * @ORM\Entity
 */
class Empleado
{
    /**
     * @var string
     *
     * @ORM\Column(name="empleado_cedula", type="string", length=13, nullable=false)
     */
    private $empleadoCedula;

    /**
     * @var string
     *
     * @ORM\Column(name="empleado_nombre", type="string", length=200, nullable=false)
     */
    private $empleadoNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="empleado_apellido", type="string", length=200, nullable=false)
     */
    private $empleadoApellido;

    /**
     * @var string
     *
     * @ORM\Column(name="empleado_correo", type="string", length=100, nullable=false)
     */
    private $empleadoCorreo;

    /**
     * @var string
     *
     * @ORM\Column(name="empleado_telefono", type="string", length=10, nullable=false)
     */
    private $empleadoTelefono;

    /**
     * @var string
     *
     * @ORM\Column(name="empleado_celular", type="string", length=10, nullable=false)
     */
    private $empleadoCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="empleado_celularjob", type="string", length=10, nullable=true)
     */
    private $empleadoCelularjob;

    /**
     * @var string
     *
     * @ORM\Column(name="empleado_direccion", type="string", length=200, nullable=false)
     */
    private $empleadoDireccion;

    /**
     * @var string
     *
     * @ORM\Column(name="empleado_nombrefoto", type="string", length=200, nullable=true)
     */
    private $empleadoNombrefoto;

    /**
     * @var string
     *
     * @ORM\Column(name="empleado_userskype", type="string", length=255, nullable=false)
     */
    private $empleadoUserskype;

    /**
     * @var integer
     *
     * @ORM\Column(name="empleado_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $empleadoId;

    /**
     * @var \AppBundle\Entity\Estado
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Estado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado", referencedColumnName="estado_id")
     * })
     */
    private $idEstado;

    /**
     * @var \AppBundle\Entity\Cargoempleado
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cargoempleado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cargo", referencedColumnName="cargoempleado_id")
     * })
     */
    private $idCargo;


}

