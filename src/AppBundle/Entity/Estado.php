<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estado
 *
 * @ORM\Table(name="estado")
 * @ORM\Entity
 */
class Estado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="estado_estado", type="integer", nullable=false)
     */
    private $estadoEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="estado_nombre", type="string", length=100, nullable=false)
     */
    private $estadoNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="estado_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $estadoId;


}

