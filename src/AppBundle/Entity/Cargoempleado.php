<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cargoempleado
 *
 * @ORM\Table(name="cargoempleado")
 * @ORM\Entity
 */
class Cargoempleado
{
    /**
     * @var integer
     *
     * @ORM\Column(name="cargoempleado_estado", type="integer", nullable=false)
     */
    private $cargoempleadoEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="cargoempleado_nombre", type="string", length=100, nullable=false)
     */
    private $cargoempleadoNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="cargoempleado_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cargoempleadoId;


}

