<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rol
 *
 * @ORM\Table(name="rol")
 * @ORM\Entity
 */
class Rol
{
    /**
     * @var integer
     *
     * @ORM\Column(name="rol_estado", type="integer", nullable=false)
     */
    private $rolEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="rol_nombre", type="string", length=100, nullable=false)
     */
    private $rolNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="rol_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $rolId;


}

