<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuario
 *
 * @ORM\Table(name="usuario", uniqueConstraints={@ORM\UniqueConstraint(name="usuario_cedula", columns={"usuario_cedula"}), @ORM\UniqueConstraint(name="usuario_correo", columns={"usuario_correo"}), @ORM\UniqueConstraint(name="usuario_telefono", columns={"usuario_telefono"}), @ORM\UniqueConstraint(name="usuario_celular", columns={"usuario_celular"})}, indexes={@ORM\Index(name="id_estado", columns={"id_estado"}), @ORM\Index(name="id_rol", columns={"id_rol"})})
 * @ORM\Entity
 */
class Usuario
{
    /**
     * @var string
     *
     * @ORM\Column(name="usuario_cedula", type="string", length=13, nullable=false)
     */
    private $usuarioCedula;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_nombre", type="string", length=200, nullable=false)
     */
    private $usuarioNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_apellido", type="string", length=200, nullable=false)
     */
    private $usuarioApellido;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_correo", type="string", length=100, nullable=false)
     */
    private $usuarioCorreo;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_telefono", type="string", length=10, nullable=false)
     */
    private $usuarioTelefono;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_celular", type="string", length=10, nullable=false)
     */
    private $usuarioCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_celularjob", type="string", length=10, nullable=true)
     */
    private $usuarioCelularjob;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_direccion", type="string", length=200, nullable=false)
     */
    private $usuarioDireccion;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_nombrefoto", type="string", length=200, nullable=true)
     */
    private $usuarioNombrefoto;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario_contrasena", type="string", length=255, nullable=false)
     */
    private $usuarioContrasena;

    /**
     * @var integer
     *
     * @ORM\Column(name="usuario_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $usuarioId;

    /**
     * @var \AppBundle\Entity\Estado
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Estado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado", referencedColumnName="estado_id")
     * })
     */
    private $idEstado;

    /**
     * @var \AppBundle\Entity\Rol
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Rol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rol", referencedColumnName="rol_id")
     * })
     */
    private $idRol;


}

