<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Testimonio
 *
 * @ORM\Table(name="testimonio", indexes={@ORM\Index(name="id_usuario", columns={"id_usuario"})})
 * @ORM\Entity
 */
class Testimonio
{
    /**
     * @var string
     *
     * @ORM\Column(name="testimonio_descripcion", type="string", length=500, nullable=false)
     */
    private $testimonioDescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="testimonio_calificacion", type="string", length=100, nullable=false)
     */
    private $testimonioCalificacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="testimonio_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $testimonioId;

    /**
     * @var \AppBundle\Entity\Usuario
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="usuario_id")
     * })
     */
    private $idUsuario;


}

