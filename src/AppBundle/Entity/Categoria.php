<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categoria
 *
 * @ORM\Table(name="categoria")
 * @ORM\Entity
 */
class Categoria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="categoria_estado", type="integer", nullable=false)
     */
    private $categoriaEstado;

    /**
     * @var string
     *
     * @ORM\Column(name="categoria_nombre", type="string", length=100, nullable=false)
     */
    private $categoriaNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="categoria_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categoriaId;


}

