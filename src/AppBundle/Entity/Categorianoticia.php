<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorianoticia
 *
 * @ORM\Table(name="categorianoticia")
 * @ORM\Entity
 */
class Categorianoticia
{
    /**
     * @var string
     *
     * @ORM\Column(name="categorianoticia_descripcion", type="string", length=300, nullable=false)
     */
    private $categorianoticiaDescripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="categorianoticia_nombre", type="string", length=200, nullable=false)
     */
    private $categorianoticiaNombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="categorianoticia_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $categorianoticiaId;


}

