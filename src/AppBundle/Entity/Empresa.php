<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Empresa
 *
 * @ORM\Table(name="empresa", uniqueConstraints={@ORM\UniqueConstraint(name="empresa_ruc", columns={"empresa_ruc"})})
 * @ORM\Entity
 */
class Empresa
{
    /**
     * @var string
     *
     * @ORM\Column(name="empresa_ruc", type="string", length=13, nullable=false)
     */
    private $empresaRuc;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa_nombre", type="string", length=400, nullable=false)
     */
    private $empresaNombre;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa_direccion", type="string", length=400, nullable=false)
     */
    private $empresaDireccion;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa_telefono", type="string", length=10, nullable=false)
     */
    private $empresaTelefono;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa_celular", type="string", length=10, nullable=false)
     */
    private $empresaCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa_email", type="string", length=100, nullable=false)
     */
    private $empresaEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa_logo", type="string", length=200, nullable=false)
     */
    private $empresaLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="empresa_enfoque", type="string", length=1000, nullable=false)
     */
    private $empresaEnfoque;

    /**
     * @var integer
     *
     * @ORM\Column(name="empresa_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $empresaId;


}

