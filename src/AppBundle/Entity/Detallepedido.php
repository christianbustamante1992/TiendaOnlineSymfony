<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Detallepedido
 *
 * @ORM\Table(name="detallepedido", indexes={@ORM\Index(name="id_pedido", columns={"id_pedido"}), @ORM\Index(name="id_producto", columns={"id_producto"})})
 * @ORM\Entity
 */
class Detallepedido
{
    /**
     * @var integer
     *
     * @ORM\Column(name="detallepedido_cantidad", type="integer", nullable=false)
     */
    private $detallepedidoCantidad;

    /**
     * @var integer
     *
     * @ORM\Column(name="detallepedido_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $detallepedidoId;

    /**
     * @var \AppBundle\Entity\Pedido
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pedido")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pedido", referencedColumnName="pedido_id")
     * })
     */
    private $idPedido;

    /**
     * @var \AppBundle\Entity\Producto
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Producto")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_producto", referencedColumnName="producto_id")
     * })
     */
    private $idProducto;


}

