<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Unirest;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/pruebarest", name="pruebarest")
     */
    public function pruebaAction(Request $request)
    {
     /*   $headers = array('Accept' => 'application/json');
        $query = array('foo' => 'hello', 'bar' => 'world');

        $response = Unirest\Request::post('http://mockbin.com/request', $headers, $query);*/

        /*$response->code;        // HTTP Status code
        $response->headers;     // Headers
        $response->body;        // Parsed body
        $response->raw_body;    // Unparsed body*/
        
        $headers = array(
                         'Accept' => 'application/json',
                         'FRAMEWORK' => 'Masterpc',
                         'USERID' => 'jbohorquez',
                         'PASSWORD' => 'jbs15',
                         'DEVICEID' => '000000000000000',
                         'VERSION' => '1.0',
                         'SESSIONID' => '0'
                        );
        //$data = array('name' => 'ahmad', 'company' => 'mashape');

        //$body = Unirest\Request\Body::json($data);

        $response = Unirest\Request::get('http://200.105.245.218:9280/appSymfony/web/app_dev.php/client/stockmatriz/mat', $headers);

        return new JsonResponse($response->body->data);
    }
}
